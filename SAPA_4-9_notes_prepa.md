# Préparation du 14/02

## Infos générales 

- **Liens** [Page de l'évènement](https://ok.hypotheses.org/3061), [Lien d'inscription](https://framaforms.org/inscription-a-la-rencontre-publique-savoirs-partages-49-archives-et-memoires-1706182825)
- **Lieu et horaire** Le 14 février 2024 de 9h à 13h et on peut manger ensemble ensuite. Bibliothèque François-Mitterrand, site Tolbiac, Quai François Mauriac Paris 13e Aquarium en Hall Est, petite salle en Haut-de-jardin
- **Contact** Tel pro Simon A : +33 6 09 95 21 45
- **Structure des interventions** : courte présentation de SAPA (5 min max) + 30 min Renaud + 30 min Alexia + discussion animée par Irène (55 min)
- **SAPA?** : [Page du séminaire](https://ok.hypotheses.org/projets/science-ouverte/sapa). Neuf séances de septembre 23 à juin 24, pour interroger le partage des savoirs à l’ère digitale dans des contextes sociaux variés, en impliquant différents acteurs : société civile, médecins, scientifiques, entrepreneur·euse·s, activitstes, autodidactes, éditeur·ice·s... 
- **Librairie [Zotero](https://www.zotero.org/groups/5398458/savoirspartages/library)**, sera alimentée au fur et à mesure

## Personnes 

### **[Irène Bastard](https://experts.bnf.fr/page_personnelle/irene-bastard)**
Chercheuse en sociologie à la BNF, son doctorat porte sur "la réception des médias et le rôle des discussions décentrées, en ligne et hors ligne, avec des proches ou non, pour une compréhension sociale de l'actualité". Travaille sur les publics de la BNF, l'accès à ses ressources papier et numérique et leur usage.

### **Simon Dumas-Primbault**
Chercheur chez [OpenEdition](https://www.openedition.org/19219). Ancien étudiant à polytechnique, bifurque vers un doctorat de philosophie des sciences portant sur les carnets de recherche de Newton, Leibniz et Viviani. Question directrice : comment les pratiques matérielles façonnent les manières de penser?, appliquée par la suite au numérique, sous forme d'une étude des [affordances](https://fr.wikipedia.org/wiki/Affordance) et des usages sociaux des plateformes de savoir (bibliothèques, science ouverte).

### **[Celya Gruson-Daniel](https://celyagd.github.io/about/)**
Praticienne-chercheuse chez [Inno3](https://inno3.fr/). Formée en science congitives et comportementales et en sciences sociales, travaille sur l'étude de l'impact du numérique sur les relations sciences/société, sur l'utilisation des données et publications scientifiques dans des régimes numériques des savoirs, et la question de l'accessibilité cognitive, à la suite des travaux de Florence Piron.  

### **Alexia-Levy Chekroun**
Inscrite en année de préparation au doctorat, à l’Ecole des Hautes Etudes en Sciences Sociales, sous la direction de Sebastien Tank-Storper (CéSor – CNRS). Travaille sur les controverses et conflits d’interprétations relatifs à la “décolonisation” du judaïsme sur Instagram et les usages politiques es formes de vie juives passées d'Afrique du Nord et du Moyen-Orient. 

### **Renaud Chantraine**
Etudie l'histoire de l'art et la muséologie puis produit une thèse sur la patrimonialisation des minorités LGBTQI et des luttes contre le VIH. Actuellement post-doctorant au SESSTIM à Marseille, il travaille sur une cartographie des différents dispositifs de fabrication, de préservation et de transmission des archives et des mémoires de la lutte contre le VIH/sida.

--- 

## **Questions communes**

Notre discussion a porté en grande partie sur des questions communes / points de recoupement à aborder en plus des points exposés séparément par chaque intervenant.e. 
L'objet général des présentations est exposé sur la [page dédiée](https://ok.hypotheses.org/3061), mais rapidement : 
- Alexia a selectionné une demi-douzaine d'utilisateurs instagram qui investissent la mémoire des juifs du moyen-orient. Elle s'inspire de la sociologie des connaissances, pour qui les savoirs sont nécessairement situés, liés aux positionnements géographiques et sociologiques différenciés. L'approche est en partie fondée sur l'autodescription des influenceurs. 
- Renaud s'est inscrit dans une démarche d'anthropologie, au départ sur la transmission des archives d'un militant gay des années 80-90 dans le contexte Marseillais, mais par la force des choses (la part versée aux archives départementales est innaccessible ces cinquantes prochaines années), l'enquête s'est trouvée être une problématisation autour de la patrimonialisation et de la possibilité d'avoir des centres d'archives communautaires.  

### De quelle mémoire, histoire, archive est-il question ? Quelle période, zone géographique, quel contexte politique, quels évènements historiques?
- Renaud : Les années 80-90 principalement en France, dans un contexte d'homophobie d'Etat, d'ostracisation sociale et d'indifférence des pouvoirs publics face à une épidémie qui cause des dizaines de milliers de morts brutales, touchant des personnes jeunes et en particulier les usagers de drogues et les hommes ayant des relations sexuelles avec des hommes.
- Alexia : Porte sur les formes de vies juives du Moyen-Orient et d'Afrique du Nord, vécues par les grands-parents et arrière grands-parents des utilisateur.ice.s d'instagram étudiés, on peut donc remonter jusqu'au début du 20e, et pour une mise en contexte plus générale, sur une histoire au temps beaucoup plus long (deux millénaires). Les évènements plus récents sont les différentes vagues de migration et d'expulsions des populations juives hors des pays du Maghreb et du Moyen-Orient dans le second 20e siècle.

### Quels enjeux contemporains autour de ces archives? En particulier quel besoin y-a-t'il à savoir et à faire savoir? L'archive fonctionne-t-elle comme preuve, comme matière d'analyse, comme support identitaire? 
- Alexia :
  - Les controverses actuelles sur l'analyse d'Israël en terme colonial, est-ce que les juifs sont blancs et est-ce que on appliquer au conflit en cours la même grille de lecture qu'à la guerre d'Algérie. Les mémoires d'Afrique du Nord viennent nourrir un narratif sur la question de l'indigénéité des juifs au territoire israélo-palestinien. 
  - La disparition des personnes juif.ve.s nées au Moyen-Orient et au Maghreb et avec elles de leur culture, leur corporéité, et leur mémoire
- Renaud : Disparition des derniers témoins, crise depuis le transfert des archives d'Act-Up aux Archives Nationales, disparition de la mémoire gay, disparition de l'archive comme outil militant. Qu'est-ce qu'une association qui se sépare de toutes ses archives? Quelle place peut jouer le numérique dans cette configuration? 

### De quoi est constitué le "fonds" d'archives en circulation dans vos études de cas?
- Alexia : Nombreux témoignages sur les trajectoires d'exil européens, par des influenceurs qui sont issus de trajectoires migratoires, mais font partie de la diaspora et qui ont un regard qui est aussi occidental. Pas mal de scans de photos familiales qui mettent en avant l'indigénéité, les costumes traditionnels yéménites par exemple.
- Renaud : beaucoup de courriers, quelques photos, beaucoup de supports de communication, et notamment des supports de communication érotiques. L'un des projets de JM Michel c'est "Santé Plaisir Gay", qui organisait des jerk-off parties. Réappropriation de la sexualité entre hommes dans un contexte de VIH, sans risque. Représentation des invitations.  

### Quels principes d'archivation et de présentation de l'archive sont à l'oeuvre, entre archive privée, spontanée, et archive publiée? 
- Alexia : Les mises en récits des auteur.ice.s de comptes instagram, pris dans le design d'interface et les algorithmes propres de ces plateformes (qui seront étudiés plus tard dans le travail de thèse)
- Renaud : L'archivage au fil de l'eau et de la vie par les militant.e.s, qui entre en friction une fois déposé dans une archive officielle avec les règles archivistiques et surtout beaucoup de manque de transparence et de flou, surtout autour des délais de traitement, du critère de confidentialité des données (code du patrimoine [L213-1](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000031971829/), [L213-2](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000043887707), tel qu'expliqué [ici](https://francearchives.gouv.fr/fr/article/26287562)). Fonds interdit aux moins de 18 ans. La question de la contractualisation est cruciale : on a pas été informé qu'il était possible de demander une dérogation, il a fallu relire le contrat de don : "le donateur ou ses représentants peuvent accéder à l'intégralité du fonds à n'importe quel moment", et insister pour voir le traitement du fonds.  

### Questions supplémentaires qui pourraient être intéressantes, qu'on a évoquée, sur l'intime, la violence, de deuil / travail de mémoire :
- Quelle est la place de la violence (en particulier celle perpétrée par l'Etat) dans ces archives? Quelle est la place de la mort, à la fois celle des derniers témoins actuels, mais aussi et surtout des morts violentes, non reconnues, du travail de deuil collectif et individuel qui n'a pas pu être fait? 
- Quel travail de mémoire est nécessaire et possible avec ces modes de circulation de l'archive? 
- Quelle est la place de l'intime, de la politisation de l'intime dans ces archives? 

### Questions supplémentaires que je suggère sur la gouvernance, les dispositifs sociotechniques : 
- La gouvernance de l'archive. Quelles parties-prenantes sont impliquées (en prenant aussi en compte les dispositifs techniques éventuels (logiciels, algorithmes)), selon quels flux et structures organisationelles ? 
- Quelle tension entre le local et le global? le temps court et le temps long? la spontanéité et la réflexivité? l'intime et le public? la grande superstructure étatique ou l'industrie des réseaux sociaux et des communs numériques décentralisés ou des pratiques de micro-archives, à petite échelle?  
- Comment comparer le fonctionnement de ces deux archives, l'une soumise à la logique patrimoniale d'état, l'autre aux règles des grandes réseaux sociaux numériques? Comment instagram permet des appropriations mémorielles que les anciens dispositifs ne permettent pas? Quels sont les effets de l'immatérialité des supports? Et à l'inverse, qu'est-ce que ces dispositifs permettent qu'instagram ne permet pas?
- Que serait une archive "parfaite"? Par quels vecteurs circulerait-elle? Dans quelle organisation sociale s'inscrirait-elle? Comment faire des archives universelles sans sacrifier le besoin de les faire aussi par et pour les communautés?


